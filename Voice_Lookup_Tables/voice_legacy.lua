--[[
    VOX API vX.X.XX (indev)
    (c) 2021 Peekofwar

    This is the sound index for the VOX voice.
    The root folder is determined by the filename of the sound index file (this file)
    

    -- The voice lines will be indexed something like this:
    {
        word = {
            path = "word", -- path to the sound file relative to the root folder
            length = 0.8, -- Length of the sound (not counting reverb) in seconds
        },
    }
]]
{
    __voiceRoot = "aci.vox.voice_legacy",
    access = {
        path = "access",
        length = 0.711,
    },
    activated = {
        path = "activated",
        length = 0.810,
    },
    activation = {
        path = "activation",
        length = 1.039,
    },
    advised = {
        path = "advised",
        length = 0.910,
    },
    alert = {
        path = "alert",
        length = 0.730,
    },
    beepboop = {
        path = "beepboop",
        length = 0.850,
    },
    bizwarn = {
        path = "bizwarn",
        length = 0.454,
    },
    buzwarn = {
        path = "buzwarn",
        length = 0.250,
    },
    check = {
        path = "check",
        length = 0.382,
    },
    coolant = {
        path = "coolant",
        length = 0.643,
    },
    deactivated = {
        path = "deactivated",
        length = 1.182,
    },
    deeoo = {
        path = "deeoo",
        length = 0.600,
    },
    denied = {
        path = "denied",
        length = 0.816,
    },
    depleted = {
        path = "depleted",
        length = 0.826,
    },
    doop = {
        path = "doop",
        length = 0.700,
    },
    excess = {
        path = "excess",
        length = 0.733,
    },
    excessive = {
        path = "excessive",
        length = 0.753,
    },
    fissile = {
        path = "fissile",
        length = 0.502,
    },
    fission = {
        path = "fission",
        length = 0.770,
    },
    ["for"] = {
        path = "for",
        length = 0.447,
    },
    fuel = {
        path = "fuel",
        length = 0.714,
    },
    granted = {
        path = "granted",
        length = 0.829,
    },
    high = {
        path = "high",
        length = 0.492,
    },
    ill = {
        path = "ill",
        length = 0.391,
    },
    insufficient = {
        path = "insufficient",
        length = 0.933,
    },
    malfunction = {
        path = "malfunction",
        length = 0.921,
    },
    meltdown = {
        path = "meltdown",
        length = 0.796,
    },
    nuclear = {
        path = "nuclear",
        length = 0.871,
    },
    overflow = {
        path = "overflow",
        length = 0.783,
    },
    radiation = {
        path = "radiation",
        length = 0.998,
    },
    reactor = {
        path = "reactor",
        length = 0.910,
    },
    status = {
        path = "status",
        length = 0.729,
    },
    steam = {
        path = "steam",
        length = 0.660,
    },
    temperature = {
        path = "temperature",
        length = 0.692,
    },
    terminal = {
        path = "terminal",
        length = 0.557,
    },
    turbine = {
        path = "turbine",
        length = 0.830,
    },
    warning = {
        path = "warning",
        length = 0.670,
    },
    waste = {
        path = "waste",
        length = 0.485,
    },
    woop = {
        path = "woop",
        length = 0.401,
    },
}
