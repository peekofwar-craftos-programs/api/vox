--[[
	VOX API by Peekofwar
	(c) 2021-2023
]]

local vox = {}
local private = {}

vox.version = "1.1"
vox.version_type = "release"
vox.version_date = "January 15th, 2023" 
vox.config = {}
vox.config.default_voice = "voice_legacy"
vox.config.voice_directory = "/voices"
vox.config.intercom_blacklist = {}
vox.config.intercom_whitelistMode = false
vox.debug = {}
vox.debug.verbose = false
vox.debug.print_errors = true

private.window = false
function vox.debug.changeWindow(window)
	if window ~= nil or window ~= false or type(window)~="table" or type(window.getSize)~="function" then error("Not a window object!",0) end
	private.window = window
	if window == false or window == nil then
		return false, "Will use current window"
	else
		return true, "Will use specified window"
	end
end

private.playQueue = {}
private.voices = {}
local wrapper = require("cc.strings").wrap
function private.winPrint(win,...)
	local tx,ty = term.getCursorPos()
	local w,h = win.getSize()
	local input = {...}
	local str = ""
	for i=1,#input do
		str = str..tostring(input[i])
		if i<#input then str = str.." " end
	end
	local text = wrapper(str,w)
	for i=1,#text do
		local x,y = win.getCursorPos()
		win.write(text[i])
		if y==h then win.scroll(1) win.setCursorPos(1,y) else win.setCursorPos(1,y+1) end
	end
	term.setCursorPos(tx,ty)
end
local verbose = {}
function verbose.print(...)
	if vox.debug.verbose then
		private.winPrint(private.window or term.current(),...)
	end
end
function verbose.printColor(color,...)
	if vox.debug.verbose then
		local win = private.window or term.current()
		local c = win.getTextColor()
		win.setTextColor(color)
		private.winPrint(win,...)
		win.setTextColor(c)
	end
end
function verbose.printError(...)
	if vox.debug.print_errors then
		local win = private.window or term.current()
		local c = win.getTextColor()
		win.setTextColor(colors.red)
		private.winPrint(win,...)
		win.setTextColor(c)
	end
end

local intercom = {}
intercom.list = {}
function intercom.findAll()
	local connectedPeripherals = peripheral.getNames()
	intercom.list = {}
	for i=1, #connectedPeripherals do
		if peripheral.getType(connectedPeripherals[i]) == "speaker" then
			if vox.config.intercom_whitelistMode and vox.config.intercom_blacklist[connectedPeripherals[i]] or not vox.config.intercom_whitelistMode and not vox.config.intercom_blacklist[connectedPeripherals[i]] then
				table.insert(intercom.list,connectedPeripherals[i])
			else
				verbose.printColor(colors.lightGray,connectedPeripherals[i].." is blacklisted")
			end
		end
	end
end
function intercom.playSound(soundName,vol,pitch)
	intercom.findAll()
	verbose.print("Playing: "..soundName)
	for i=1, #intercom.list do
		local device = peripheral.wrap(intercom.list[i])
		device.playSound(soundName,vol or 1,pitch or 1)
	end
end

function vox.queue(list)
	if type(list.playlist) == "table" and #list.playlist > 0 then
		verbose.print('Queueing playlist with '..#list.playlist..' words...')
		table.insert(private.playQueue,list)
	elseif type(list.sentence) == "string" then
		verbose.printColor(colors.red,"Converting to table...")
		local list_converted={}
		for str in string.gmatch(list.sentence, "([^".." ".."]+)") do
			table.insert(list_converted, str)
		end
		local message = {}
		for i=1, #list_converted do
			message[i] = {}
			local word = list_converted[i]
			local trailChar = string.sub(word,#word,#word)
			if trailChar == "." or trailChar == "!" or trailChar == "?" then
				message[i].pause = "1"
				word = string.sub(word,1,#word-1)
			elseif trailChar == "," or trailChar == ";" or trailChar == ":" then
				message[i].pause = "0.25"
				word = string.sub(word,1,#word-1)
			end
			message[i].word = word
		end
		
		list.playlist = message
		
		verbose.print('Queueing playlist with '..#list.playlist..' words...')
		table.insert(private.playQueue,list)
	else
		verbose.printError("Error: Playlist malformed!")
	end
	os.queueEvent('vox_run')
end
function private.loadVoice(voice_name)
	if private.voices[voice_name] then return true, "Already loaded." end
	local request = vox.config.voice_directory.."/"..voice_name..".lua"
	if not fs.exists(request) then
		return false, "Could not find requested voice '"..voice_name.."'"
	end
	local file = fs.open(request,"r")
	private.voices[voice_name] = textutils.unserialise(file.readAll())
	file.close()

	if not private.voices[voice_name].__voiceRoot then
		return false, "'__voiceRoot' not defined!"
	end
	return true, "Voice loaded."
end
function vox.run()
	while #private.playQueue > 0 do
		local playlist = private.playQueue[1].playlist
		local voice = private.playQueue[1].voice or vox.config.default_voice
		if type(playlist) == "table" then
			verbose.print("Selected voice: "..voice)
			local ok, msg = private.loadVoice(voice)
			verbose.print(msg)
			if ok then
				verbose.print('Playing playlist with '..#playlist..' sounds...')
				for i=1, #playlist do
					playlist[i].word = string.lower(playlist[i].word)
					if private.voices[voice][playlist[i].word] then
						intercom.playSound(private.voices[voice].__voiceRoot.."."..private.voices[voice][playlist[i].word].path,1,1)
						sleep(private.voices[voice][playlist[i].word].length + (playlist[i].pause or 0))
					else
						verbose.printError("Word '"..playlist[i].word.."' not defined in selected voice")
					end
				end
			else
				verbose.printError("Couldn't load voice... Aborted!")
			end
			verbose.print("Removing 1 of "..#private.playQueue.." queued playlists...")
			table.remove(private.playQueue,1)
		end
	end
end

return vox