--[[
	VOX System Program
	(c) 2021-2025 Peekofwar
]]
local program_info = false
program_info = {
	path = shell.getRunningProgram(),
	extension = shell.getRunningProgram():match("[^%.]*$"),
	name = string.sub(shell.getRunningProgram(),1,#shell.getRunningProgram()-#shell.getRunningProgram():match("[^%.]*$")-1),
	appName = 'ACI VOX System',
	version = {
		string = "1.0.5",
		date = "January 13th, 2025",
		build = 1,
	},
	files = 
	{
		config = "/.program_data/vox/config.lua",
		config_old = string.sub(shell.getRunningProgram(),1,#shell.getRunningProgram()-#shell.getRunningProgram():match("[^%.]*$")-1) ..'.cfg',
	   -- voice_dir = string.sub(shell.getRunningProgram(),1,#shell.getRunningProgram()-#shell.getRunningProgram():match("[^%.]*$")-1).."/voices",
		voice_dir = "/voices",
	},
	help = {
		display = function()
			term.setCursorPos(1,1) term.clear()
			local w, h = term.getSize()
			local helpScreen = window.create(term.current(),1,1,w,h-1)
			local sw, sh = helpScreen.getSize()
			local lines = {
				{colors.yellow,program_info.appName},
				"v"..program_info.version.string.." build "..program_info.version.build.." ("..program_info.version.date..")",
				"",
				{colors.lightBlue,"About:"},
				"Plays sounds over the intercom upon requests sent ",
				"from networked computers.",
				"",
				{colors.lightBlue,"Usage:"},
				"Send a message over the network with the following",
				"table:",
                {colors.green,"{"},
                {colors.green,"   message = \"VOX_REQUEST\","},
                {colors.green,"   voice = \"voice_legacy\","},
                {colors.green,"   sentence = \"doop: Hello, world!\","},
                {colors.green,"   playlist = {"},
                {colors.green,"     {"},
                {colors.green,"         word = \"doop\","},
                {colors.green,"         pause = 0.25,"},
                {colors.green,"     },"},
                {colors.green,"     {"},
                {colors.green,"         word = \"hello\","},
                {colors.green,"     },"},
                {colors.green,"     {"},
                {colors.green,"         word = \"world\","},
                {colors.green,"     },"},
                {colors.green,"   },"},
                {colors.green,"}"},
            -- "|                                                    |"
                "'message' informs the program that this is infact a",
                "vox message request.",
                "'word' tells the system what word is to be spoken.",
                "'pause' optionally forces the VOX to add an extra",
                "delay before playing the next word.",
                "'voice' specifies the desired voice. Will default",
                "to the configured default voice.",
				"",
				"You can choose to either send a 'playlist' to",
				"further customize the delays more finely, or use a",
				"'sentence' instead to leave the processing to the",
				"VOX intercom system. If both are present, only the",
				"'playlist' key will be used.",
                "",
                {colors.lightBlue,"Switches:"},
                " /dev   - Activates dev functions (unused)",
                " /debug - Triggers debugging keybinds (unused)",
                " /verbose - Triggers additional output (unused)",
                " /test - Opens the voice test/scan menu",
                "",
            -- "|                                                    |"
				{colors.lightBlue,"Debugging hotkeys:"},
				" F9 - Triggers crash screen",
			}
			local scroll = 1
			local scrollMax = #lines-sh+1
			if #lines <= sh then scrollMax = 1 end
			while true do
				scroll = math.clamp(1,scrollMax,scroll)
				term.setCursorPos(1,h)

				term.setTextColor(colors.yellow)
				term.write("Use ")
				if scroll == 1 then term.setTextColor(colors.gray) end
				term.write("/\\")
				term.setTextColor(colors.yellow)
				if scroll == scrollMax then term.setTextColor(colors.gray) end
				term.write(" \\/")
				term.setTextColor(colors.yellow)
				term.write(" to scroll or press ENTER to quit.")

				helpScreen.setCursorPos(1,1)
				helpScreen.setTextColor(colors.white)
				helpScreen.clear()
				for i=scroll, sh+scroll do
					helpScreen.setTextColor(colors.white)
					if lines[i] == nil then break end
					if type(lines[i]) == "table" then
						helpScreen.setTextColor(lines[i][1])
						helpScreen.write(lines[i][2])
					else
						helpScreen.write(lines[i])
					end
					local x,y = helpScreen.getCursorPos()
					helpScreen.setCursorPos(1,y+1)
				end
				helpScreen.setCursorPos(1,h)
				helpScreen.write(scroll..","..sh+scroll)
				local event, key = os.pullEvent("key")
				if key == keys.up then
					scroll = scroll-1
					term.setCursorPos(1,h)
				elseif key == keys.down then
					scroll = scroll+1
					term.setCursorPos(1,h)
				elseif key == keys.enter or key == keys.numPadEnter then break end
			end
			term.clear()
			term.setCursorPos(1,1)
			error()
		end,
	},
}
local program_settings = {
	default_voice = "voice_legacy",
	modem_channel = 39934,
}
function math.clamp(vMin,vMax,x)
	return math.max(math.min(x,vMax),vMin)
end
local function cWrite(text)
	local w, h = term.getSize()
	local cX,cY = term.getCursorPos()
	term.setCursorPos(math.floor(w / 2 - text:len() / 2 + .5), cY)
	io.write(text)
end
local args = false
args = {
	commandLine = {...},
	scanCommandLine = function()
		for i=1, #args.commandLine do
			--print(i, type(args.commandLine[i]), args.commandLine[i])
			if string.lower(args.commandLine[i]) == "/dev" then args.dev = true end
			if string.lower(args.commandLine[i]) == "/debug" then args.debug = true end
			if string.lower(args.commandLine[i]) == "/help" then args.help = true end
			if string.lower(args.commandLine[i]) == "/?" then args.help = true program_info.help.display() end
			if string.lower(args.commandLine[i]) == "/voxtest" then args.voxTest = true end
			if string.lower(args.commandLine[i]) == "/test" then args.test = true end
		end
		--sleep(1)
	end,
}
args.scanCommandLine()
local function test_mode()

	local root_vox_sound_id = "aci.vox"

	write("Enter voice name: ") local voice_name = read()
	print(program_info.files.voice_dir.."/"..voice_name)
	sleep(1)
	--local voice = require("/"..program_info.files.voice_dir.."/"..voice_name)
	vox.loadVoice(voice_name)
	local voice = vox.voices[voice_name]
	local voice_word_count = 0
	local voice_word_count_unique = 0
	local voice_word_unique_list = {}
	if type(voice) ~= "table" then error("Failed to load lookup table! Expected a table, got '"..type(voice).."' instead") end
	print("Beginning test...")
	printError(type(voice))
	for k,v in pairs(voice) do
		term.setCursorPos(1,1)
		term.clear()
		if type(v) == "table" then
			print("Voice: " .. voice_name or "NOT FOUND!")
			print("Word: "..k or "NOT FOUND!")
			print("Length: "..voice[k].length or "NOT FOUND!")
			local sound_id = root_vox_sound_id .. "." .. voice_name .. "." .. voice[k].path
			print("Sound ID: " .. sound_id or "NOT FOUND!")
			if not voice_word_unique_list[sound_id] then voice_word_unique_list[sound_id] = true voice_word_count_unique = voice_word_count_unique+1 end
			
			intercom.playSound(sound_id)

			sleep(voice[k].length)
			voice_word_count = voice_word_count + 1
		end
	end
	print("\n~~FINISHED~~\n" .. voice_word_count .. " words in selected voice ("..voice_word_count_unique.." unique)")
	error(nil,0)
end
dev = {
	print = print,
	printError = printError,
}
intercom = {
	list = {},
	findAll = function()
		local connectedPeripherals = peripheral.getNames()
		intercom.list = {}
		for i=1, #connectedPeripherals do
			if peripheral.getType(connectedPeripherals[i]) == "speaker" then
				table.insert(intercom.list,connectedPeripherals[i])
			end
		end
	end,
	playSound = function(soundName,vol,pitch)
		intercom.findAll()
		dev.print("Playing: "..soundName)
		for i=1, #intercom.list do
			local device = peripheral.wrap(intercom.list[i])
			device.playSound(soundName,vol or 1,pitch or 1)
		end
	end,
}
intercom.findAll()
local setup = function()
	print(program_info.appName)
	print("Version "..program_info.version.string.." build "..program_info.version.build.. " ("..program_info.version.date..")")
	
	program_settings.modem = peripheral.find("modem")
	program_settings.modem.open(program_settings.modem_channel)
	return program_settings.modem and true or false
end
local pretty = require("cc.pretty")

local function awaitMessage()
	if not setup() then error("Couldn't find a modem!",0) end
	while true do
		local event_data = {os.pullEvent()}
		if event_data[1] == "modem_message" then
			--dev.printError("Received modem message")
			local request = event_data[5]
			if request.message == "VOX_REQUEST" then
				term.setTextColor(colors.lightBlue)
				dev.print("Received VOX_REQUEST")
				term.setTextColor(colors.white)
				vox.queue(request)
			end
		end
	end
end

vox = {
	playQueue = {},
	voices = {},
	queue = function(list)
		if type(list.playlist) == "table" and #list.playlist > 0 then
			print('Queueing playlist with '..#list.playlist..' words...')
			table.insert(vox.playQueue,list)
		elseif type(list.sentence) == "string" then
			printError("Converting to table...")
			local list_converted={}
			for str in string.gmatch(list.sentence, "([^".." ".."]+)") do
				table.insert(list_converted, str)
			end
			local message = {}
			for i=1, #list_converted do
				message[i] = {}
				local word = list_converted[i]
				local trailChar = string.sub(word,#word,#word)
				if trailChar == "." or trailChar == "!" or trailChar == "?" then
					message[i].pause = "1"
					word = string.sub(word,1,#word-1)
				elseif trailChar == "," or trailChar == ";" or trailChar == ":" then
					message[i].pause = "0.25"
					word = string.sub(word,1,#word-1)
				end
				message[i].word = word
			end
			
			list.playlist = message
			
			print('Queueing playlist with '..#list.playlist..' words...')
			table.insert(vox.playQueue,list)
		else
			printError("Error: Playlist malformed!")
		end
		os.queueEvent('vox_run')
	end,
	loadVoice = function(voice_name)
		if vox.voices[voice_name] then return true, "Already loaded." end
		local request = program_info.files.voice_dir.."/"..voice_name..".lua"
		if not fs.exists(request) then
			return false, "Could not find requested voice '"..voice_name.."'"
		end
		local file = fs.open(request,"r")
		vox.voices[voice_name] = textutils.unserialise(file.readAll())
		file.close()

		if not vox.voices[voice_name].__voiceRoot then
			return false, "'__voiceRoot' not defined!"
		end
		return true, "Voice loaded."
	end,
	run = function()
		while true do
			term.setTextColor(colors.green)
			print("\nAwaiting request...")
			term.setTextColor(colors.white)

			os.pullEvent('vox_run')

			--dev.print(pretty.print(pretty.pretty(vox.playQueue)))
			while #vox.playQueue > 0 do
				local playlist = vox.playQueue[1].playlist
				local voice = vox.playQueue[1].voice or program_settings.default_voice
				if type(playlist) == "table" then
					dev.print("Selected voice: "..voice)
					local ok, msg = vox.loadVoice(voice)
					dev.print(msg)
					if ok then
						dev.print('Playing playlist with '..#playlist..' sounds...')
						for i=1, #playlist do
							playlist[i].word = string.lower(playlist[i].word)
							if vox.voices[voice][playlist[i].word] then
								intercom.playSound(vox.voices[voice].__voiceRoot.."."..vox.voices[voice][playlist[i].word].path,1,1)
								sleep(vox.voices[voice][playlist[i].word].length + (playlist[i].pause or 0))
							else
								printError("Word '"..playlist[i].word.."' not defined in selected voice")
							end
						end
					else
						printError("Couldn't load voice... Aborted!")
					end
					dev.print("Removing 1 of "..#vox.playQueue.." queued playlists...")
					table.remove(vox.playQueue,1)
				end
			end
		end
	end,
}

if args.test then test_mode() end
if args.help then program_info.help.display() end
local pass, err = pcall(function() parallel.waitForAll(awaitMessage,vox.run) end)
if err then printError(err) end
if program_settings.modem then program_settings.modem.close(program_settings.modem_channel) end
